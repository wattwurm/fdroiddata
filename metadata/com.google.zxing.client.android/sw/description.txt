Soma maandishi zilizo andikwa kwa barcodes au jenga QR-codes za ku patiana addresses,  contacti, ama maandishi kati kati za divisi.

Kama njia zote zimeshindikana na unaamini shida ni apu, tuma ujumbe kwa developa na maelezo yanayo fuata. Ujumbe bila maelezo hayata angaliwa, maanisha yata puuzwa.
